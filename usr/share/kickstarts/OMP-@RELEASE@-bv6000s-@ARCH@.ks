# DisplayName: OMP bv6000s/@ARCH@ (release) 0.2.2
# KickstartType: release
# DeviceModel: bv6000s
# DeviceVariant: bv6000s
# Brand: omp
# Var@aurora-hw@vendor: mediatek
# SuggestedFeatures: omp-base, sailfish-eas, aurora
# SuggestedImageType: loop
# SuggestedArchitecture: armv7hl

timezone --utc UTC
keyboard us
lang en_US.UTF-8
user --name nemo --groups audio,input,video --password nemo

### Commands from /tmp/sandbox/usr/share/ssu/kickstart/part/bv6000s
part / --fstype="ext4" --size=3500 --label=root
part /home --fstype="ext4" --size=800 --label=home
part /fimage --fstype="ext4" --size=10 --label=fimage

## No suitable configuration found in /tmp/sandbox/usr/share/ssu/kickstart/bootloader

repo --name=adaptation0-bv6000s-@RELEASE@ --baseurl=https://releases-internal.omprussia.ru/releases/@RELEASE@/omp-hw/adaptation-mediatek-bv6000s/@ARCH@/ --priority=50
repo --name=adaptation1-bv6000s-@RELEASE@ --baseurl=https://releases-internal.omprussia.ru/releases/@RELEASE@/omp-hw/adaptation-mediatek-bv6000s-dhd/@ARCH@/ --priority=50
repo --name=apps-@RELEASE@ --baseurl=https://releases-internal.omprussia.ru/releases/@RELEASE@/omp-apps/@ARCH@/ --priority=50
repo --name=omp-@RELEASE@ --baseurl=https://releases-internal.omprussia.ru/releases/@RELEASE@/omp/@ARCH@/ --priority=50

%packages
@OMP Configuration bv6000s
%end

%attachment
### Commands from /tmp/sandbox/usr/share/ssu/kickstart/attachment/bv6000s
/boot/hybris-boot.img
/boot/hybris-recovery.img
/boot/update-binary
/boot/*scatter.txt
/boot/*README.txt
/boot/*.bin
/boot/*.gz
/boot/*.tar*
/boot/fastboot*
/boot/*.img
/boot/flash.sh
/etc/hw-release
droid-config-bv6000s-out-of-image-files

%end

%pre
export SSU_RELEASE_TYPE=release
### begin 01_init
touch $INSTALL_ROOT/.bootstrap
### end 01_init
### begin 10_signed-rpm
if [ "@EXTRA_NAME@" == "signed" ] || [ "@EXTRA_NAME@" == "signed-sboot" ]; then
  # Install RPM sign key
  RELEASEPATTERN2=$(echo @RELEASEPATTERN@ | cut -d'.' -f 1-3)
  zypper ar -G https://repo.omprussia.ru/pj:/oss"$RELEASEPATTERN2":/@BRAND@:/@RNDFLAVOUR@/@RNDRELEASE@_i486/ oss-KEY

  zypper -n in omp-keys-packages-public
  rpm -ih /omphook-*.rpm
  mkdir -p $INSTALL_ROOT/var/lib/ompcert
  mkdir -p $INSTALL_ROOT/etc/rpm
  cp /etc/rpm/{rootcacert-omp.pem,system-developer-keyid} $INSTALL_ROOT/etc/rpm
fi
### end 10_signed-rpm
%end

%post
export SSU_RELEASE_TYPE=release
### begin 01_arch-hack
if [ "@ARCH@" == armv7hl ] || [ "@ARCH@" == armv7tnhl ]; then
    # Without this line the rpm does not get the architecture right.
    echo -n "@ARCH@-meego-linux" > /etc/rpm/platform

    # Also libzypp has problems in autodetecting the architecture so we force tha as well.
    # https://bugs.meego.com/show_bug.cgi?id=11484
    echo "arch = @ARCH@" >> /etc/zypp/zypp.conf
fi
### end 01_arch-hack
### begin 01_rpm-rebuilddb
# Rebuild db using target's rpm
echo -n "Rebuilding db using target rpm.."
rm -f /var/lib/rpm/__db*
rpm --rebuilddb
echo "done"
### end 01_rpm-rebuilddb
### begin 50_oneshot
# exit boostrap mode
rm -f /.bootstrap

# export some important variables until there's a better solution
export LANG=en_US.UTF-8
export LC_COLLATE=en_US.UTF-8
export GSETTINGS_BACKEND=gconf

# run the oneshot triggers for root and first user uid
UID_MIN=$(grep "^UID_MIN" /etc/login.defs |  tr -s " " | cut -d " " -f2)
DEVICEUSER=`getent passwd $UID_MIN | sed 's/:.*//'`

if [ -x /usr/bin/oneshot ]; then
   /usr/bin/oneshot --mic
   su -c "/usr/bin/oneshot --mic" $DEVICEUSER
fi
### end 50_oneshot
### begin 60_ssu
if [ "$SSU_RELEASE_TYPE" = "rnd" ]; then
    [ -n "@RNDRELEASE@" ] && ssu release -r @RNDRELEASE@
    [ -n "@RNDFLAVOUR@" ] && ssu flavour @RNDFLAVOUR@
    # RELEASE is reused in RND setups with parallel release structures
    # this makes sure that an image created from such a structure updates from there
    [ -n "@RELEASE@" ] && ssu set update-version @RELEASE@
    ssu mode 2
else
    [ -n "@RELEASE@" ] && ssu release @RELEASE@
    ssu mode 4
fi
### end 60_ssu
### begin 98_cert
[ -x /usr/sbin/aide ] && /usr/sbin/aide -V9 -C
rm -f /etc/pki/tls/gsch
rm -f /home/nemo/.pathfinderssl/gsch.key
rm -f /root/.pathfinderssl/gsch.key
[ ! -f /etc/aide.conf ] || sed -i -e "s/^\/dev\/mmcblk0/#NO SECUREBOOT# \/dev\/mmcblk0/" /etc/aide.conf
rm -f /var/log/sdjd.log
### end 98_cert
### begin 99_omp-domain
if [ "$SSU_RELEASE_TYPE" = "rnd" ]; then
    ssu domain omp
fi
### end 99_omp-domain
%end

%post --nochroot
export SSU_RELEASE_TYPE=release
### begin 50_os-release
(
CUSTOMERS=$(find $INSTALL_ROOT/usr/share/ssu/features.d -name 'customer-*.ini' \
    |xargs --no-run-if-empty sed -n 's/^name[[:space:]]*=[[:space:]]*//p')

cat $INSTALL_ROOT/etc/os-release
echo "SAILFISH_CUSTOMER=\"${CUSTOMERS//$'\n'/ }\""
) > $IMG_OUT_DIR/os-release
### end 50_os-release
### begin 60_checksum
if [ "@EXTRA_NAME@" == "signed" ] || [ "@EXTRA_NAME@" == "signed-sboot" ]; then
    echo "### Package checksums ###"

    COUNTER=0
 
    for filename in $(find /var/tmp/mic/cache/packages/ -name *.rpm); do
        ufix $filename && COUNTER=$[$COUNTER +1]
    done

    echo "### Total checksums count $COUNTER ###"
fi
### end 60_checksum
%end

%pack
export SSU_RELEASE_TYPE=release
### begin hybris
### begin hybris
pushd $IMG_OUT_DIR

MD5SUMFILE=md5.lst

DEVICE_VERSION_FILE=./hw-release
if [ -n "@EXTRA_NAME@" ] && [ "@EXTRA_NAME@" != @"EXTRA_NAME"@ ]; then
  EXTRA_NAME="@EXTRA_NAME@-"
fi


if [[ -a $DEVICE_VERSION_FILE ]]; then
  source $DEVICE_VERSION_FILE
  DEVICE_ID=-${MER_HA_DEVICE// /_}-$VERSION_ID
fi

source ./os-release
if [ "$SSU_RELEASE_TYPE" = "rnd" ]; then
  RND_FLAVOUR=$SAILFISH_FLAVOUR
fi

RELEASENAME=${NAME// /_}-${SAILFISH_CUSTOMER// /_}${SAILFISH_CUSTOMER:+-}${EXTRA_NAME// /_}$RND_FLAVOUR${RND_FLAVOUR:+-}$VERSION_ID$DEVICE_ID

IMGSECTORS=0
IMGBLOCKS=0
IMGSIZE=0
BLOCKSIZE=0

resizeloop() {
  local IMG=$1
  local LOOP=$(/sbin/losetup -f)
  /sbin/losetup $LOOP $IMG
  /sbin/e2fsck -f -y $LOOP

  # Get image blocks and free blocks
  local BLOCKCOUNT=$(/sbin/dumpe2fs -h $LOOP 2>&1 | grep "Block count:" | grep -o -E '[0-9]+')
  local FREEBLOCKS=$(/sbin/dumpe2fs -h $LOOP 2>&1 | grep "Free blocks:" | grep -o -E '[0-9]+')
  BLOCKSIZE=$(/sbin/dumpe2fs -h $LOOP 2>&1 | grep "Block size:" | grep -o -E '[0-9]+')
  echo "$IMG total block count: $BLOCKCOUNT - Free blocks: $FREEBLOCKS"

  local IMAGEBLOCKS=$(/usr/bin/expr $BLOCKCOUNT - $FREEBLOCKS)
  local IMAGESECTORS=$(/usr/bin/expr $IMAGEBLOCKS \* $BLOCKSIZE / 512 )

  # Shrink to minimum
  echo "Shrink $IMG to $IMAGESECTORS"
  /sbin/resize2fs $LOOP ${IMAGESECTORS}s -f

  # Get the size after resize.
  IMGBLOCKS=$(/sbin/dumpe2fs -h $LOOP 2>&1 | grep "Block count:" | grep -o -E '[0-9]+')
  IMGSIZE=$(/usr/bin/expr $IMGBLOCKS \* $BLOCKSIZE)
  IMGSECTORS=$(/usr/bin/expr $IMGBLOCKS \* $BLOCKSIZE / 512)
  echo "$IMG resized block count: $IMGBLOCKS - Block size: $BLOCKSIZE - Sectors: $IMGSECTORS - Total size: $IMGSIZE"

  /sbin/losetup -d $LOOP
}

echo "Resize root and home"

# Resize root and home to minimum
resizeloop root.img

ROOTSIZE=$IMGSIZE
ROOTBLOCKS=$IMGBLOCKS
ROOTSECTORS=$IMGSECTORS

resizeloop home.img

HOMESIZE=$IMGSIZE
HOMEBLOCKS=$IMGBLOCKS
HOMESECTORS=$IMGSECTORS

# We will add some (100M) extra to temp image.
TEMPIMGSECTORS=$(/usr/bin/expr $ROOTSECTORS + $HOMESECTORS + 200000 )

dd if=/dev/zero bs=512 count=0 of=temp.img seek=$TEMPIMGSECTORS
LVM_LOOP=$(/sbin/losetup -f)
/sbin/losetup $LVM_LOOP temp.img
/usr/sbin/pvcreate $LVM_LOOP
/usr/sbin/vgcreate sailfish $LVM_LOOP

echo "Create logical volume ROOT size: $ROOTSIZE"
/usr/sbin/lvcreate -L ${ROOTSIZE}B --name root sailfish

echo "Create logical volume HOME size: $HOMESIZE"
/usr/sbin/lvcreate -L ${HOMESIZE}B --name home sailfish

/bin/sync
/usr/sbin/vgchange -a y sailfish

dd if=root.img bs=$BLOCKSIZE count=$ROOTBLOCKS of=/dev/sailfish/root

dd if=home.img bs=$BLOCKSIZE count=$HOMEBLOCKS of=/dev/sailfish/home

# Extra name for factory setup
EXTRANAME=$(echo "@EXTRA_NAME@" | tr '[:upper:]' '[:lower:]')

case "x$EXTRANAME" in
  xfactory*)

    mkdir root home
    mount /dev/sailfish/root root
    mount /dev/sailfish/home home

    case "x$EXTRANAME" in
      xfactory-en*)
        ROOT_PATH=root/ HOME_PATH=home/nemo/ bash setup-factory-boot.sh --english
        ;;
      *)
        ROOT_PATH=root/ HOME_PATH=home/nemo/ bash setup-factory-boot.sh
        ;;
    esac
    
    umount home
    umount root

    rm -rf home root
    ;;
esac

rm -f setup-factory-boot.sh

/usr/sbin/vgchange -a n sailfish
pigz -7 root.img && md5sum -b root.img.gz > root.img.gz.md5&
pigz -7 home.img && md5sum -b home.img.gz > home.img.gz.md5&
wait

ROOTSIZE=$(/bin/ls -l root.img.gz | cut -d ' ' -f5)
HOMESIZE=$(/bin/ls -l home.img.gz | cut -d ' ' -f5)

# Temporary dir for making factory image backups.
FIMAGE_TEMP=$(mktemp -d -p $(pwd))

# For some reason loop files created by imager don't shrink properly when
# running resize2fs -M on them. Hence manually growing the loop file here
# to make the shrinking work once we have the image populated.
dd if=/dev/zero bs=512 seek=$(/usr/bin/expr \( $ROOTSIZE + $HOMESIZE + 100000000 \) / 512) count=1 of=fimage.img
/sbin/e2fsck -f -y fimage.img
/sbin/resize2fs -f fimage.img

mount -o loop fimage.img $FIMAGE_TEMP
mkdir -p $FIMAGE_TEMP/${RELEASENAME}
mv root.img.gz* $FIMAGE_TEMP/${RELEASENAME}
mv home.img.gz* $FIMAGE_TEMP/${RELEASENAME}
umount $FIMAGE_TEMP
rmdir $FIMAGE_TEMP

/sbin/e2fsck -f -y fimage.img
/sbin/resize2fs -f -M fimage.img

# To make the file magic right lets convert to single file sparse image.
/usr/bin/img2simg fimage.img fimage.img001
rm fimage.img

/sbin/losetup -d $LVM_LOOP

mv temp.img sailfish.img

/usr/bin/atruncate sailfish.img
# To make the file magic right lets convert to single file sparse image.
# 3456M size is selected so that we always will have only one file as output.
/usr/bin/img2simg sailfish.img sailfish.img001
rm sailfish.img

# Create empty partition for cache that can be replaced later
dd if=/dev/zero bs=1M count=10 of=cache.img
/sbin/mkfs.ext4 -F cache.img

chmod 755 flash.*

# Unpacking boot images
if ls *.gz >&/dev/null; then
  gunzip *.gz
fi

FILES="flash* *.img* *.mbn *.bin *.dat *.xtt *-release patch0.xml rawprogram0.xml *scatter.txt *.tar*"
FLASHING_FILES="*.img* *.mbn *.bin *.dat *.img *.xtt *-release patch0.xml rawprogram0.xml *scatter.txt"
FILES_TO_COPY="*.urls"

mkdir -p ${RELEASENAME}
cp ${FILES_TO_COPY} ${RELEASENAME}/

case "x$EXTRANAME" in
  x*flashing*)
    mv ${FLASHING_FILES} ${RELEASENAME}/
    rm -f ${FILES}

    cd ${RELEASENAME}
    # Convert image
    python ../checksparse.py -i rawprogram0.xml -o rawprogram_unsparse.xml
    rm -f ../checksparse.* fimage.img001 persist.img sailfish.img001 rawprogram0.xml
    cd ..
    ;;
  *)
    rm -f checksparse.*

    mv ${FILES} ${RELEASENAME}/
    ;;
esac

# Calculate md5sums of files included to the tarball
cd ${RELEASENAME}
md5sum * > $MD5SUMFILE
cd ..

# Package stuff back to tarball
RELEASE_ARCHIVE="${RELEASENAME}.tar.bz2"
tar -cjf "${RELEASE_ARCHIVE}" $RELEASENAME
md5sum ${RELEASENAME}.tar.bz2 > $MD5SUMFILE

# Remove the files from the output directory
rm -r ${RELEASENAME}

popd
### end hybris
### end hybris
%end

