
= FLASHING =

Before starting flashing on any host turn off the device. After this follow the
instructions given for your host PC operating system.

By this point of time you should already have the .tar.bz2 file that contains
the image as this flashing instructions file that you are reading at the moment
is inside that .tar.bz2 file. As a general note the flashing can take a long
time (>10 minutes) and it flashes image with similar name multiple times in the
end which is expected behaviour.

== Unlocking bootloader ==

* Press the power button and volume +.

* Choose the menu item fastboot: press "volume +" and then "volume -".

* Open terminal application and execute command: fastboot oem unlock.

* Press "volume -". 

== LINUX ==

Way 1: SP Flash Tool (first flashing).

* Open terminal application and go to the folder where the image is extracted.

* Extract flashing tool: tar xvf SP_Flash_Tool_*.tar

* Launch flashing tool from terminal: cd SP_Flash_Tool_v5.1728_Linux/ ; chmod +x flash_tool ; ./flash_tool

* Open Download tab.

* Select Download-Agent: MTK_AllInOne_DA.bin

* Select Scatterloading-file: MT6737T_Sailfish_OS_scatter.txt

* Press Download button. 

* Turn off the device. 

* Plug the device to the host. 

* Wait until the process is complete.

* Detact usb cable, press and hold the powerkey to reboot.

* The device should boot up to new Sailfish OS.

Way 2: Flashing script.

Open terminal application and go to the folder where the image is extracted.

Next:
* Hold device so that USB connector is up.

* Next start flashing script by entering following command:

  bash ./flash.sh

* Enter your password if requested to gain root access for flashing the device
* Once flashing is completed you will see text: 

  "Flashing completed. Detact usb cable, press and hold the powerkey to reboot."

* After following the guidance from script device should boot up to new Sailfish OS

NOTE: If flashing does not succeed, you might have missing fastboot binary or
it is too old. Many distros include andoid-tools package, but that might not
be new enough to support device flashing.

Installation commands for some linux distributions:
* Ubuntu: sudo apt-get install android-tools-fastboot

If you want to compile fastboot binary for your distro you can compile version
5.1.1 release 38 or newer from:
https://github.com/mer-qa/qa-droid-tools